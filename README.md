#Porte Plumes Citations

**Plugin en dev**

 Permettre de rajouter une référence, de citer un auteur...

 Voir https://git.spip.net/spip/forum/issues/4780

Au départ mon besoin était surtout de pouvoir citer un auteur dans un forum. La discussion a précisé les normes à respecter, qui sont bien résumées dans cet article : https://css-tricks.com/quoting-in-html-quotations-citations-and-blockquotes/

Pour l'instant trois boutons sont rajoutés à la barre d'édition sous `
<quote>
` :
- Citer un auteur balise `<figcaption>`
- Citer une référence balise `<cite>`
- une citation courte avec la balise `<q>`

## Configuration
- Il est possible d'activer cette barre pour les forums
- Il est possible de choisir quels outils on affiche
## Todo

- S'assurer que tout ceci est bien compréhensible pour les rédacteurs
- Trouver des icones plus adaptées
- Ajouter l'attribut cite `<q cite="https://johnrhea.com/summons">` afin d'afficher des url (voir aussi comment l'afficher en css)