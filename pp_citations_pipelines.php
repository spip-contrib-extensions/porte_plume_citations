<?php

/**
 * Utilisations de pipelines par porte_plume_citations
 *
 * @plugin     porte_plume_citations
 * @copyright  2024
 * @author     Jacques Bouthier
 * @licence    GNU/GPL
 * @package    SPIP\Pp_citations\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

	
function pp_citations_porte_plume_barre_pre_charger($barres) {
	// on ajoute les boutons dans les 2 barres de SPIP
	foreach (['edition','forum'] as $nom) {
		$barre = &$barres[$nom];

	$barre->ajouterPlusieursApres('barre_poesie', [
				// citer_auteur 	
			[
				'id'          => 'citer_auteur',
				'name'        => _T('pp_citations:outil_inserer_citer_auteur'),
				'className'   => 'outil_citer_auteur', 
				'openWith'    => '<figcaption>',
				'closeWith'   => '</figcaption>',
				'display'     => true,
				'selectionType' => 'line',
			],

				// <cite> 	
			[
				'id'          => 'balise_cite',
				'name'        => _T('pp_citations:outil_inserer_balise_cite'),
				'className'   => 'outil_balise_cite', 
				'openWith'    => '<cite>',
				'closeWith'   => '</cite>',
				'display'     => true,
				'selectionType' => 'line',
			],
			
				// q tag 	
			[
				'id'          => 'q_tag',
				'name'        => _T('pp_citations:outil_inserer_q_tag'),
				'className'   => 'outil_q_tag', 
				'openWith'    => '<q>',
				'closeWith'   => '</q>',
				'display'     => true,
				'selectionType' => 'line',
			],
		]);
	}
	return $barres;

	return $barres;
}

function pp_citations_porte_plume_barre_charger($barres) {
	// en fonction de la config, afficher ou non sur barre d'edition et forum
	// par defaut : edition = oui, forum = non
	// ce que donne deja pre_charger par ailleurs
	$pp = isset($GLOBALS['meta']['porte_plume']) ? @unserialize($GLOBALS['meta']['porte_plume']) : '';

	if (isset($pp['citations']) and $citations = $pp['citations']) {
		$activer = [];

		if ($citations['activer_barre_edition'] == 'on') {
			$activer[] = 'edition';
		}
		if ($citations['activer_barre_forum'] == 'on') {
			$activer[] = 'forum';
		}
		foreach ($activer as $nom) {
			if (isset($barres[$nom])) {
				$barre = &$barres[$nom];

				$outils_actifs = (isset($citations['outils_actifs']) and is_array($citations['outils_actifs'])) ? $citations['outils_actifs'] : [];
				if ($outils_actifs) {
					$barre->afficher($outils_actifs);
					$barre->afficher(['sepGuillemets', 'quote']);
				}
			}
		}
	}
	return $barres;
}

function pp_citations_porte_plume_lien_classe_vers_icone($flux) {

	return array_merge($flux, array(
		'outil_citer_auteur'   => 'citer_auteur.svg',
		'outil_balise_cite'   => 'balise_cite.svg',
		'outil_q_tag'   => 'q_tag.svg',
		
		
	));
}